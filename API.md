# API Guidelines

We use REST and all of our APIs are RESTful. However, note that "the term _REST_ is used throughout this document to mean services that are in the spirit of REST rather than adhering to REST by the book".

> To provide the smoothest possible experience for developers on platforms following the REST API Guidelines, REST APIs SHOULD follow consistent design guidelines to make using them easy and intuitive.

The REST "original whitepaper" is a MUST read ([Representational State Transfer (REST)](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm)) along with HTTP/1.1 semantics ([RFC 7231](https://tools.ietf.org/html/rfc7231)). Additionally, our APIs MUST follow [OpenAPI Specs (currently v3)](http://spec.openapis.org/oas/v3.0.2), so they are also a MUST read. Yeah, we know it's long, but we are making good quality web services!

All APIs SHOULD be versioned, and we will be using URL versioning and [semver](https://semver.org), with only major values prepended by a `v`. Initial API versions MUST be named `0` and SHOULD be considered unstable and subject to change greatly. I.E.: initial version is named as `v0`, whereas the first stable version is `v1`. All applications that consume from an API MUST implement dynamic versioning and NOT hardcode version in the URL. The corresponding API version MAY be obtained from configs or from another API. Additional versioning such as minor and/or patch MAY be used.  
Regarding deprecation and maintenance, at least 2 stable versions SHOULD be maintained, and every other previous version MAY be deprecated. The existence of a new major version SHOULD be clear indication of incoming deprecation for older versions.

Additionally, the [Microsoft REST API Guidelines](https://github.com/Microsoft/api-guidelines/blob/master/Guidelines.md) are a very good example of what a well-designed API means: chapters 1 to 8 are taken mostly as-is, the rest of the chapters might not be followed exactly in the same way. Currently, this guideline is quite vague and on purpose: many things are yet-to-be-defined, so in case of doubt, ask your co-workers or team leader.

## Resources

* [Learn REST: A RESTful Tutorial](https://www.restapitutorial.com/)
* [Microsoft REST API Guidelines](https://github.com/Microsoft/api-guidelines/blob/master/Guidelines.md)
