# Docker Guidelines

For whatever it is you need to do, check first if there is a base image to achieve that. There are a number of [base images](https://gitlab.com/explore/projects?tag=docker-base-image) to be used for most projects created by us.

As a general guideline, always create a limited user and drop privileges before calling the entrypoint and the command. Use `copy --chown` that can now expand envs and args.

Use envs and args properly: if your variable is not required at runtime (container) then use an arg, otherwise an env.

Lint your Dockerfile with [hadolint](https://github.com/hadolint/hadolint). Additionally, configure your pipeline to do it for you (if you started from a skel repo, then it's already done).

Try to use multi-stage builds, where in the first stage you install a ton of dependencies to build whatever it is you need to build, and then in the next stage you just use what you've just built. This reduces the size and complexity of resulting images.

Follow OCI Image Format Specification for [annotation keys](https://github.com/opencontainers/image-spec/blob/master/annotations.md).

## Security

Check your Dockerfile with some static code analyzer such as [Snyk](https://snyk.io/test/) which can be easily run as a [docker container](https://hub.docker.com/r/snyk/snyk-cli/#docker-1). Configure your pipeline to automate this (if you started from a skel repo, then it's already done).

### Signed Images

We are working to get all of our images signed, but it is currently not implemented. You can help us out with it, just ask your team leader about this.

### Resources

* [10 Docker Image Security Best Practices](https://snyk.io/blog/10-docker-image-security-best-practices/)
