# Python Guidelines

Every Python developer must comply to these guidelines. When in doubt on how to solve a problem, always ask your coworkers or your team leader.

Code review is highly recommended for all projects and might be enforced by repository settings.

Before coding a new library, navigate the repository and ask your coworkers if one such library already exists, internally or externally. Try to improve internal libraries whenever possible. Try very hard not to break compatibility (use versioning correctly).  
You can and are encouraged to write free/libre software libraries (as long as no company secret is exposed), ask a coworker or your team leader to know more about this.

**Index**

* [Version](#version)
   * [pyenv](#using-pyenv-tldr)
* [Versioning](#versioning)
* [Package Manager](#package-manager)
* [Coding](#coding)
* [Testing](#testing)
* [Documenting](#documenting)
* [Settings](#settings)
* [Tasks](#tasks)
* [Frameworks](#frameworks)
   * [FastAPI](#fastapi)
   * [Django](#django)
   * [Guidelines for both](#guidelines-for-both)
      * [Overview](#overview)
      * [Starting projects](#starting-projects)
      * [Models](#models)
         * [Custom validation](#custom-validation)
         * [Properties](#properties)
         * [Methods](#methods)
         * [Testing](#testing-1)
      * [Services](#services)
         * [Naming convention](#naming-convention)
      * [Selectors](#selectors)
         * [Naming convention](#naming-convention-1)
         * [FastAPI Crud](#fastapi-crud)
      * [APIs & Serializers](#apis-serializers)
         * [Naming convention](#naming-convention-2)
      * [Urls](#urls)
      * [Exception Handling](#exception-handling)
         * [Raising Exceptions in Services / Selectors](#raising-exceptions-in-services-selectors)
         * [Handle Exceptions in APIs](#handle-exceptions-in-apis)
         * [Error formatting](#error-formatting)
      * [Testing](#testing-2)
         * [Naming conventions](#naming-conventions)
         * [Testing services](#testing-services)
         * [Testing selectors](#testing-selectors)
* [HTTP Server](#http-server)
* [Utils](#utils)
* [IDE](#ide)
* [MyPy](#mypy)
* [YAPF](#yapf)

## Version

Python ^3.8+ is mandatory (v4 is to be discussed when its arrival is near).  
Projects might only maintain one Python version: we try to avoid unnecessary effort for keeping old versions working, so we are usually near the edge. However, only stable versions are allowed in prod, which means no alphas, betas, or rc's. You can use them for testing purposes or to try new features, but you can't use such features until it lands in a stable release.

This doesn't mean that for each Python release you have to run out to update every project. It simply means that current ongoing projects have to be updated to the version of choice, checked that nothing breaks and then push the change. For other projects their update time might or might not come, there's no rush.

You can use [Pyenv](https://github.com/pyenv/pyenv) to handle different Python versions locally if you need to (this is of course not mandatory).

### Using PyEnv: tl;dr

1. Install pyenv: `git clone https://github.com/pyenv/pyenv.git ~/.pyenv` (this means that to update pyenv simply `cd ~/.pyenv && git pull --prune` and to uninstall just `rm -rf ~/.pyenv`).
2. Define `PYENV_ROOT` (see [its repo](https://github.com/pyenv/pyenv#basic-github-checkout) for other shells):

```
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
```

3. Optionally enable shims and autocompletion: `echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile` (again, check its repo for other shells). Restart your shell after this.
4. Pyenv builds each installed python version, thus requiring [python build deps](https://github.com/pyenv/pyenv/wiki#suggested-build-environment). For Debian/Ubuntu/Mint: `sudo apt-get update && sudo apt-get install --no-install-recommends make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev`

Now pyenv is ready to go. For any required Python version, just install it once: `pyenv install <version>` (i.e. `pyenv install 3.8.3`). Run `pyenv install --list` to see all supported versions or `pyenv install -l | grep <version>` (remember to update pyenv every once in a while, specially if you don't find the wanted version).

Once you have your wanted version, create a virtualenv for your project with `pyenv virtualenv <version> <project name>`, i.e.: `pyenv virtualenv 3.8.3 fastapi-skel`. To activate it you can run `pyenv activate fastapi-skel` or better go to the project directory and run instead `pyenv local fastapi-skel` to set it as the virtualenv for said project, and it will activate automatically whenever you enter the directory and deactivate when you leave it.

That's it!.

### Resources

* [Managing Multiple Python Versions With pyenv](https://realpython.com/intro-to-pyenv/)

## Versioning

Use [semver](https://semver.org/). Some projects may use [calver](https://calver.org/) but ask your team leader first.

## Package Manager

Use [Poetry](https://poetry.eustace.io/). Only use [PyProject.toml](https://www.python.org/dev/peps/pep-0518/) and **not** *setup.py/.cfg*.

## Coding

All code must comply with [PEP8](https://www.python.org/dev/peps/pep-0008/), [PEP257](https://www.python.org/dev/peps/pep-0257/), [PEP287](https://www.python.org/dev/peps/pep-0287/) and implement [PEP484](https://www.python.org/dev/peps/pep-0484/) as much as possible.  Write RST docstrings.

Use single quotes instead of double quotes for strings, unless the string itself contains a single quote.  
Write lines no longer than 89 characters (around 90 is typically OK, we simply choose 89 to set our tooling). Some projects may specify lines a little bit longer but the hard limit is 95.

Python code is beatiful. Write idiomatic code and type-hint everything!

### Quality

[Cyclomatic Complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity) MUST be below 10 (B), and SHOULD be 5 or lower (A). Use [mccabe](https://github.com/PyCQA/mccabe) as a plugin for *flake8* to enforce it with the `max-complexity` setting. Use [radon](https://radon.readthedocs.io/en/latest/) for better analysis and information.

### Tips

Generators are a very powerful and fast resource in Python. Prefer Generators when the resources are simply going to be looped once. Use list comprehension instead when a result or several loopings are needed.  
Filter is blazing fast if you use it as a Generator but converting it to a list or tuple can be slower than using list comprehension directly, so be toughtful about it.

### Resources

* [Intro to Using Python Type Hints](https://kishstats.com/python/2019/01/07/python-type-hinting.html)
* [The Best flake8 Extensions for your Python Project](https://julien.danjou.info/the-best-flake8-extensions/)
* [Example on how to document your Python docstrings](https://thomas-cokelaer.info/tutorials/sphinx/docstring_python.html)
* [Transforming Code into Beautiful, Idiomatic Python](https://www.youtube.com/watch?v=OSGv2VnC0go)

## Testing

All code MUST pass at least 60% of coverage for APIs and more than 80% for libraries or modules.

You SHOULD use **pytest** as test framework and therefore use test functions. You MAY use the standard Python UnitTest lib and create test classes: use `TestCase` classes for sync tests or `IsolatedAsyncioTestCase` classes for async tests.

You CAN use [Tox](https://tox.readthedocs.io/en/latest/).  
You CAN use [mock](https://docs.python.org/3/library/unittest.mock.html).  

To *mock* prefer `MagicMock` class over simple `Mock`, its more powerful and easier to use. To mock an awaitable use `AsyncMock` class instead.

### Resources

* [pytest docs](https://docs.pytest.org/en/latest/)
* [Advanced Python Testing](https://joshpeak.net/posts/2019-06-18-Advanced-python-testing.html)
* [Testing and Code coverage with Python](https://developer.ibm.com/recipes/tutorials/testing-and-code-coverage-with-python/)
* [An Introduction to Mocking in Python](https://www.toptal.com/python/an-introduction-to-mocking-in-python)
* [IsolatedAsyncioTestCase](https://docs.python.org/3/library/unittest.html#unittest.IsolatedAsyncioTestCase)
* [AsyncMock](https://docs.python.org/3/library/unittest.mock.html#unittest.mock.AsyncMock)

## Documenting

Besides correctly documenting code (remember we use RST docstrings), write proper documentation in each project's wiki such as diagrams, functional analysis, usage guides and examples, etc.

### Resources

* [Documenting Python Code: A Complete Guide](https://realpython.com/documenting-python-code/)

## Settings

Prefer env vars to customize application settings so that deploys can be easily automated. This of course depends on the specific case. I.e. for FastAPI use Pydantic's BaseSettings (which is already done in the skel).

## Tasks

For programmatic/automated tasks like commands use [Invoke](https://www.pyinvoke.org/). **Don't use Makefile!**.

## Frameworks

For APIs use [FastAPI](https://fastapi.tiangolo.com/) unless there are more gains using [Django with Rest Framework](https://www.django-rest-framework.org/) (i.e.: it has modules for what you need to accomplish directly), but ask your coworkers and team leader first.

Check the repository for [project skeletons](https://gitlab.com/nevrona/devs/skels) and also our [public skels](https://gitlab.com/nevrona/public/skels/fastapi). Every project MUST begin from one of the skels if any.

### FastAPI

Use [SQLAlchemy](https://sqlalchemy-utils.readthedocs.io/) exclusively. There MUST be several good reasons to use another one instead and MUST be approved by the head of your department.

For faster JSON responses you can use `orjson` as [documented](https://fastapi.tiangolo.com/advanced/custom-response/#use-orjsonresponse). DO NOT use `ujson` since it hasn't been updated in years!.

The skel is properly documented on where should the code live, but if any doubt arises or docs are not enough, ask a coworker or team leader and help to expand them.

### Django

Use [Rest Framework](https://www.django-rest-framework.org/) and Django's ORM exclusively.

If you have to hack your way through obscure Django settings and functions to get what you need, maybe it wasn't the right choice, and you should switch to FastAPI.

### Guidelines for both

Based on the [HackSoft Django Styleguide](https://github.com/HackSoftware/Django-Styleguide) we picked several concepts which are copied below, and edited to fit both Django and FastAPI frameworks (things not mentioned have thereby no specific rule). Items beginning with `[Django]` are exclusively for Django and those beginning with `[FastAPI]` are exclusively for FastAPI.

Note that the following is oriented to those frameworks, yet the rest of the Python guidelines present here have precedence.

#### Overview

**Business logic should live in:**

* Model properties (with some exceptions).
* [Django] Model `clean` method for additional validations (with some exceptions).
* Services - functions, that take care of writing to the database.
* Selectors - functions, that take care of fetching from the database.

**Business logic should not live in:**

* APIs and Views.
* Serializers and Forms.
* Form tags.
* Model `save` method.

**Model properties vs selectors:**

* If the model property spans multiple relations, it should better be a selector.
* If a model property, added to some list API, will cause `N + 1` problem that cannot be easily solved with `select_related`, it should better be a selector.

#### Starting projects

Use an existing skel as mentioned above.

#### Models

Let's take a look at an example model (even though it's a Django model, the same concepts goes for SQLAlchemy):

```python
class Course(models.Model):
    name = models.CharField(unique=True, max_length=255)

    start_date = models.DateField()
    end_date = models.DateField()

    attendable = models.BooleanField(default=True)

    students = models.ManyToManyField(
        Student,
        through='CourseAssignment',
        through_fields=('course', 'student')
    )

    teachers = models.ManyToManyField(
        Teacher,
        through='CourseAssignment',
        through_fields=('course', 'teacher')
    )

    slug_url = models.SlugField(unique=True)

    repository = models.URLField(blank=True)
    video_channel = models.URLField(blank=True, null=True)
    facebook_group = models.URLField(blank=True, null=True)

    logo = models.ImageField(blank=True, null=True)

    public = models.BooleanField(default=True)

    generate_certificates_delta = models.DurationField(default=timedelta(days=15))

    objects = CourseManager()

    def clean(self):
        if self.start_date > self.end_date:
            raise ValidationError('End date cannot be before start date!')

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)

    @property
    def visible_teachers(self):
        return self.teachers.filter(course_assignments__hidden=False).select_related('profile')

    @property
    def duration_in_weeks(self):
        weeks = rrule.rrule(
            rrule.WEEKLY,
            dtstart=self.start_date,
            until=self.end_date
        )
        return weeks.count()

    @property
    def has_started(self):
        now = get_now()

        return self.start_date <= now.date()

    @property
    def has_finished(self):
        now = get_now()

        return self.end_date <= now.date()

    @property
    def can_generate_certificates(self):
        now = get_now()

        return now.date() <= self.end_date + self.generate_certificates_delta

    def __str__(self) -> str:
        return self.name
```

Few things to spot here.

**[Django] Custom validation:**

* There's a custom model validation, defined in `clean()`. This validation uses only model fields and spans no relations.
* This requires someone to call `full_clean()` on the model instance. The best place to do that is in the `save()` method of the model. Otherwise, people can forget to call `full_clean()` in the respective service.

**Properties:**

* All properties, except `visible_teachers`, work directly on model fields.
* `visible_teachers` is a great candidate for a **selector**.

We have few general rules for custom validations & model properties / methods:

##### Custom validation

* [Django] If the custom validation depends only on the **non-relational model fields**, define it in `clean` and call `full_clean` in `save`.
* [FastAPI] If the custom validation depends only on the **non-relational model fields**, define it in the model CRUD for *create* and *update*.
* If the custom validation is more complex & **spans relationships**, do it in the service that creates the model.
* [Django] It's OK to combine both `clean` and additional validation in the `service`.
* [FastAPI] It's OK to combine both CRUD and additional validation in the `service`.
* [Django] If you can do a validation using [Django's constraints](https://docs.djangoproject.com/en/2.2/ref/models/constraints/), then you should aim for that. Less code to write.
* [FastAPI] If you can do a validation using [constraints](https://docs.sqlalchemy.org/en/13/core/constraints.html), then you should aim for that. Less code to write.

##### Properties

* If your model properties use only **non-relational model fields**, they are OK to stay as properties.
* If a property, such as `visible_teachers` starts **spanning relationships**, it's better to define a selector for that.

##### Methods

* If you need a method that updates several fields at once (for example - `created_at` and `created_by` when something happens), you can create a model method that does the job.
* Every model method should be wrapped in a service. There should be no model method calling outside a service.

##### Testing

Models need to be tested only if there's something additional to them - like custom validation or properties.

If we are strict & don't do custom validation / properties, then we can test the models without actually writing anything to the database => we are going to get quicker tests.

For example, if we want to test the custom validation, here's how a test could look like:

```python
from datetime import timedelta

from django.test import TestCase  # use unittest.TestCase for FastAPI
from django.core.exceptions import ValidationError

from project.common.utils import get_now

from project.education.factories import CourseFactory
from project.education.models import Course


class CourseTests(TestCase):
    def test_course_end_date_cannot_be_before_start_date(self):
        start_date = get_now()
        end_date = get_now() - timedelta(days=1)

        course_data = CourseFactory.build()
        course_data['start_date'] = start_date
        course_data['end_date'] = end_date

        course = Course(**course_data)

        with self.assertRaises(ValidationError):
            course.full_clean()
```

There's a lot going on in this test:

* `get_now()` returns a timezone aware datetime.
* `CourseFactory.build()` will return a dictionary with all required fields for a course to exist.
* We replace the values for `start_date` and `end_date`.
* We assert that a validation error is going to be raised if we call `full_clean`.
* We are not hitting the database at all, since there's no need for that.

Here's how `CourseFactory` looks like:

```python
class CourseFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: f'{n}{faker.word()}')
    start_date = factory.LazyAttribute(
        lambda _: get_now()
    )
    end_date = factory.LazyAttribute(
        lambda _: get_now() + timedelta(days=30)
    )

    slug_url = factory.Sequence(lambda n: f'{n}{faker.slug()}')

    repository = factory.LazyAttribute(lambda _: faker.url())
    video_channel = factory.LazyAttribute(lambda _: faker.url())
    facebook_group = factory.LazyAttribute(lambda _: faker.url())

    class Meta:
        model = Course

    @classmethod
    def _build(cls, model_class, *args, **kwargs):
        return kwargs

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        return create_course(**kwargs)
```

#### Services

A service is a simple function that:

* Lives in `your_app/api/v<X_Y>/services/<service_name>.py` module.
* Takes keyword-only arguments.
* Is type-annotated (even if you are not using [`mypy`](https://github.com/python/mypy) at the moment).
* Works mostly with models & other services and selectors.
* Does business logic - from simple model creation to complex cross-cutting concerns, to calling external services & tasks.
* [FastAPI] Is always `async` even if it does nothing asynchronous.

An example service that creates a user:

```python
def create_user(
    *,
    email: str,
    name: str
) -> User:
    user = User(email=email)
    user.full_clean()
    user.save()

    create_profile(user=user, name=name)
    send_confirmation_email(user=user)

    return user
```

As you can see, this service calls 2 other services - `create_profile` and `send_confirmation_email`.

##### Naming convention

Naming conventions depend on your taste. It pays off to have a consistent naming convention throughout a project.

If we take the example above, our service is named `create_user`. The pattern is - `<action>_<entity>`.

What we usually prefer in our projects, again, depending on taste, is `<entity>_<action>` or with the example above: `user_create`. This seems odd at first, but it has few nice features:

* Namespacing. It's easy to spot all services starting with `user_` and it's a good idea to put them in a `users.py` module.
* Greppability. Or in other words, if you want to see all actions for a specific entity, just grep for `user_`.

A full example would look like this:

```python
def user_create(
    *,
    email: str,
    name: str
) -> User:
    user = User(email=email)
    user.full_clean()
    user.save()

    profile_create(user=user, name=name)
    confirmation_email_send(user=user)

    return user
```

#### Selectors

A selector is a simple function that:

* Lives in `your_app/api/v<X_Y>/selectors/<selector_name>.py` module.
* Takes keyword-only arguments.
* Is type-annotated (even if you are not using [`mypy`](https://github.com/python/mypy) at the moment).
* Works mostly with models & other services and selectors.
* Does business logic around fetching data from your database.
* [FastAPI] Is always `async` even if it does nothing asynchronous.

An example selector that lists users from the database:

```python
def get_users(*, fetched_by: User) -> Iterable[User]:
    user_ids = get_visible_users_for(user=fetched_by)

    query = Q(id__in=user_ids)

    return User.objects.filter(query)
```

As you can see, `get_visible_users_for` is another selector.

##### Naming convention

Read the section in services. Same rules apply here.

#### [FastAPI] Crud

A CRUD is a class that:

* Lives in `your_app/crud/<crud_name>.py` module.
* Takes keyword-only arguments, with one exception: the database parameter in the constructor may be positional (better readability).
* Is type-annotated (even if you are not using [`mypy`](https://github.com/python/mypy) at the moment).
* Is isolated and only works directly with the ORM.
* Is always `async` even if it does nothing asynchronous.
* Does very light or no business logic, similar to what is defined for a model.
* Handles models queries.

It is basically an ORM wrapper to be used by both services and selectors.

An example base CRUD for any model could be:

```python
"""CRUD base class."""

import typing
from abc import ABC

from pydantic import BaseModel
from sqlalchemy import and_
from sqlalchemy.orm import Query
from sqlalchemy.orm import Session

from ..db import Base
from ..db import TBase
from ..models import Account as AccountModel


class Crud(ABC):
    """Manager base class for db models.

    Extend and define class property `model` with the corresponding db model
    class.
    """

    __slots__ = (
        'db',
        '_db_obj',
    )

    model: TBase

    def __init__(self, db: Session, *, db_obj: typing.Optional[Base] = None):
        """Manage a db model.

        :param db: Request Session db.
        :param db_obj: [Optional] Database object to manage.
        """
        self.db: Session = db
        self.db_obj: typing.Optional[Base] = db_obj
        if not hasattr(self, 'model'):
            raise AttributeError('Please define the "model" attribute')

    @property
    def db_obj(self) -> typing.Optional[Base]:
        """Get database object."""
        return self._db_obj

    @db_obj.setter
    def db_obj(self, obj: Base, /) -> None:
        """Set database object."""
        self._db_obj = obj

    async def _create_before_save(self, **kwargs) -> None:
        """Additional create function for expansion before saving the object.

        Implement this method to do additional actions during creation process.
        """

    async def _create_after_save(self, **kwargs) -> None:
        """Additional create function for expansion after saving the object.

        Implement this method to do additional actions during creation process.
        """

    async def create(self, **kwargs) -> Base:
        """Create a new db object.

        Created object is stored in this object.

        :return: DB object.
        """
        self.db_obj: Base = self.model(**kwargs)
        await self._create_before_save(**kwargs)
        self.db.add(self.db_obj)
        self.db.commit()
        self.db.refresh(self.db_obj)
        await self._create_after_save(**kwargs)
        return self.db_obj

    async def read(
            self,
            *,
            offset: int = 0,
            limit: int = 20,
            one: bool = False,
            one_or_none: bool = False,
            filters: typing.Optional[typing.List] = None,  # Don't know which type is
            filter_combination=None,  # Don't know which type is
            joins: typing.List[typing.Any] = None,  # Don't know which type is
            query: typing.Optional[Query] = None,
            return_query: bool = False,
    ) -> typing.Union[None, Base, typing.List[Base], Query]:
        """Retrieve one or more objects from the db if any or the query.

        If `one` or `one_or_none` is True, retrieved object is stored in this object.

        :param offset: Query offset (only applied when querying several values).
        :param limit: Objects amount limit (only applied when querying several
                      values).
        :param one: [Optional] Retrieve only one object or raise exception.
        :param one_or_none: [Optional] Retrieve only one object (or None) or raise
                            exception if multiple results found.
        :param filters: [Optional] Search filters as a list.
        :param filter_combination: [Optional] Filter combination function from
                                   SQLAlchemy, such as `or_`, `and_`, etc
                                   (defaults to `and_`).
        :param joins: [Optional] List of joins to apply to the query.
        :param query: [Optional] Query to execute and to which apply given
                      parameters if any (joins, filters, one, offset, limit).
                      Use this parameter to execute a custom query.
        :param return_query: [Optional] Instead of executing the query, return it.
                             This means that query executors are not applied
                             (such as `one` or `one_or_none`).

        :return: One or more db objects if any or the query.

        :raises sqlalchemy.orm.exc.MultipleResultsFound: One object required
                                                         but many found.
        """
        if query is None:
            query = self.db.query(self.model)

        if joins:
            for join in joins:
                query = query.join(join)

        if filters:
            filter_combination = filter_combination if filter_combination else and_
            query = query.filter(filter_combination(*filters))

        if return_query:
            return query

        elif one_or_none:
            values = query.one_or_none()
            self.db_obj = values
        elif one:
            values = query.one()
            self.db_obj = values
        else:
            query = query.offset(offset).limit(limit)
            values = query.all()

        return values

    async def _update_before_save(self, **kwargs) -> None:
        """Additional update function for expansion before saving the object.

        Implement this method to do additional actions during update process.
        """

    async def _update_after_save(self, **kwargs) -> None:
        """Additional update function for expansion after saving the object.

        Implement this method to do additional actions during update process.
        """

    async def update(self, **kwargs) -> Base:
        """Update an existing db object.

        Note that the object must be already fetched, and will be properly updated.

        :return: Updated DB object.
        """
        if self.db_obj is None:
            raise ValueError('db_obj not set')

        if kwargs:
            for field, value in kwargs.items():
                if hasattr(self.db_obj, field):
                    setattr(self.db_obj, field, value)
        await self._update_before_save(**kwargs)
        self.db.add(self.db_obj)
        self.db.commit()
        self.db.refresh(self.db_obj)
        await self._update_after_save(**kwargs)
        return self.db_obj

    async def delete(self) -> Base:
        """Delete an existing db object.

        Note that the object must be already fetched.

        :return: Deleted DB object.
        """
        if self.db_obj is None:
            raise ValueError('db_obj not set')

        self.db.delete(self.db_obj)
        self.db.commit()
        return self.db_obj

    async def delete_bulk(
            self,
            *,
            filters: typing.List,  # Don't know which type is
            filter_combination=None,  # Don't know which type is
    ) -> None:
        """Execute a delete query with filters for bulk deletion of objects.

        :param filters: Search filters as a list.
        :param filter_combination: [Optional] Filter combination function from
                                   SQLAlchemy, such as `or_`, `and_`, etc
                                   (defaults to `and_`).
        """
        query: Query = await self.read(
            filters=filters,
            filter_combination=filter_combination,
            return_query=True,
        )
        query.delete()
        self.db.commit()
```

#### APIs & Serializers

When using services & selectors, all of your APIs should look simple & identical.

General rules for an API is:

* [FastAPI] Do 1 API per operation. For CRUD on a model, this means 4 APIs.
* [Django] Because Django does not support more than one view per URL you will have to write all operations in a single class, or two: one for creating+listing (list view) and another one for reading+updating+deleting (detail view).
* [Django] Use the most simple `APIView` or `GenericAPIView`.
* Use services / selectors & **don't do business logic in your API**.
* Use serializers for fetching objects from params - passed either via `GET` or `POST`.
* Serializers lives in `your_app/api/v<X_Y>/schemas/<module>.py` and are named representatively such as `UserCreate` or if you need to differentiate input and output it could be `UserCreateRequest` and `UserCreateResponse`. Some COULD live under `your_app/schemas/<module.py>`, but be careful about this since most schemas are naturally attached to an API version.
  * [Django] Input and Output serializer can subclass `ModelSerializer`, if needed. Prefer plain `Serializer`.
  * [FastAPI] Serializers always subclass `pydantic.BaseModel`.
  * Reuse serializers as little as possible.
  * [Django] If you need a nested serializer, use the `inline_serializer` util.

##### Naming convention

[Django] For our APIs we use the following naming convention: `<Entity><Action>Api`.  
Here are few examples: `UserCreateApi`, `UserSendResetPasswordApi`, `UserDeactivateApi`, etc.

[FastAPI] For our APIs we use the following naming convention: `<action>_<entity>` (looks better in the docs).  
Here are few examples: `create_user`, `send_reset_password`, `deactivate_user`, etc.


##### [Django] An example list API

```python
class CourseReadResponse(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'name', 'start_date', 'end_date')
```

```python
class CourseListApi(SomeAuthenticationMixin, APIView):
    serializer_class = CourseReadResponse

    def get(self, request):
        courses = get_courses()

        serializer = self.serializer_class(courses, many=True)

        return Response(serializer.data)
```

##### [FastAPI] An example list API

```python
class CourseReadResponse(pydantic.BaseModel):
    id: int
    name: str
    start_date: datetime.date
    end_date: datetime.date
```

```python
@router.get('/', response_model=schemas.CourseReadResponse)
async def list_courses(
        db: Session = Depends(get_db),
) -> typing.List[models.course.Course]:
    courses = await get_courses(db=db)
    return courses
```

##### [Django] An example detail API

```python
class CourseDetailApi(SomeAuthenticationMixin, APIView):
    serializer_class = schemas.CourseReadResponse

    def get(self, request, course_id):
        course = get_course(id_=course_id)

        serializer = self.serializer_class(course)

        return Response(serializer.data)
```

##### [FastAPI] An example detail API

```python
@router.get('/{course_id}/', response_model=schemas.CourseReadResponse)
async def read_course(
        db: Session = Depends(get_db),
        course_id: int = Path(..., gt=0),
) -> models.course.Course:
    course = await get_course(db=db, id_=course_id)
    return course
```

##### [Django] An example create API

```python
class CourseCreateRequest(serializers.Serializer):
    name = serializers.CharField()
    start_date = serializers.DateField()
    end_date = serializers.DateField()

```

```python
class CourseCreateApi(SomeAuthenticationMixin, APIView):
    serializer_class = schemas.CourseCreateRequest

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        create_course(**serializer.validated_data)

        return Response(status=status.HTTP_201_CREATED)
```

##### [FastAPI] An example create API

```python
class CourseCreateRequest(pydantic.BaseModel):
    name: str
    start_date: datetime.date
    end_date: datetime.date
```

```python
@router.post('/', status_code=status.HTTP_201_CREATED)
async def create_course(
        db: Session = Depends(get_db),
        course_data: schemas.CourseCreateRequest,
) -> None:
    await create_course(db=db, **course_data.dict())
```

##### [Django] An example update API

```python
class CourseUpdateRequest(serializers.Serializer):
    name = serializers.CharField(required=False)
    start_date = serializers.DateField(required=False)
    end_date = serializers.DateField(required=False)

```

```python
class CourseUpdateApi(SomeAuthenticationMixin, APIView):
    serializer_class = schemas.CourseUpdateRequest

    def put(self, request, course_id):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        update_course(course_id=course_id, **serializer.validated_data)

        return Response(status=status.HTTP_200_OK)
```

##### [FastAPI] An example update API

```python
class CourseUpdateRequest(pydantic.BaseModel):
    name: str
    start_date: datetime.date
    end_date: datetime.date
```

```python
@router.put('/{course_id}/', status_code=status.HTTP_200_OK)
async def create_course(
        db: Session = Depends(get_db),
        course_id: int = Path(..., gt=0),
        course_data: schemas.CourseUpdateRequest,
) -> None:
    await update_course(db=db, course_id=course_id, **course_data.dict())
```

##### [Django] Nested serializers

In case you need to use a nested serializer, you can do the following thing:

```python
class Serializer(serializers.Serializer):
    weeks = inline_serializer(many=True, fields={
        'id': serializers.IntegerField(),
        'number': serializers.IntegerField(),
    })
```

The implementation of `inline_serializer` can be:

```python
def inline_serializer(*, fields, data=None, **kwargs):
    serializer_class = create_serializer_class(name='', fields=fields)

    if data is not None:
        return serializer_class(data=data, **kwargs)

    return serializer_class(**kwargs)
```

#### Urls

We usually organize our urls the same way we organize our APIs - 1 url per API, meaning 1 url per action.  Except for Django that doesn't support more than one API per URL.

[Django] A general rule of thumb is to split urls from different domains in their own `domain_patterns` list & include from `urlpatterns`.

[Django] Here's an example with the APIs from above:

```python
from django.urls import path, include

from project.education.apis import (
    CourseListApi,
    CourseDetailApi,
    CourseSpecificActionApi,
)


course_patterns = [
    path('', CourseListApi.as_view(), name='list'),
    path('<int:course_id>/', CourseDetailApi.as_view(), name='detail'),
    path(
        '<int:course_id>/specific-action/',
        CourseSpecificActionApi.as_view(),
        name='specific-action',
    ),
]

urlpatterns = [
    path('courses/', include((course_patterns, 'courses'))),
]
```

[FastAPI] The auto routing capabilities of the skel takes care for this, so we don't have to do it :). All endpoints living in `api/v<X_Y>/endpoints/courses.py` will have the path `courses/` prepended automatically.

**Splitting urls like that can give you the extra flexibility to move separate domain patterns to separate modules**, especially for huge projects, where you'll often have merge conflicts in `urls.py`.

#### Exception Handling

##### Raising Exceptions in Services / Selectors

Now we have separation between our HTTP interface & the core logic of our application.

In order to keep this separation of concerns, our services and selectors must not use the `rest_framework.exception` classes because they are bounded with HTTP status codes.

Our services and selectors must use one of:

* [Python built-in exceptions](https://docs.python.org/3/library/exceptions.html)
* [Django] Exceptions from `django.core.exceptions`
* Custom exceptions, inheriting from the ones above.

Additionally, docstrings SHOULD indicate which exceptions are directly raised.

[Django] Here is a good example of service that performs some validation and raises `django.core.exceptions.ValidationError`:

```python
from django.core.exceptions import ValidationError


def create_topic(*, name: str, course: Course) -> Topic:
    """Create a new topic for given course.

    :raises ValidationError: Course ended and can't receive new topics.
    """
    if course.end_date < timezone.now():
       raise ValidationError('You can not create topics for a course that has ended.')

    topic = Topic.objects.create(name=name, course=course)

    return topic
```

[FastAPI] Here is a good example of service that performs some validation and raises a custom `ValidationError`:

```python
from your_app.exceptions import ValidationError


async def create_topic(*, db: Session, name: str, course: Course) -> Topic:
    """Create a new topic for given course.

    :raises ValidationError: Course ended and can't receive new topics.
    """
    if course.end_date < timezone.now():
       raise ValidationError('You can not create topics for a course that has ended.')

    topic = await crud.Topic(db).create(name=name, course=course)

    return topic
```

As you can see, both codes are practically the same except for the exception class. This makes services an selectors very portable between frameworks.

##### Handle Exceptions in APIs

In order to transform the exceptions raised in the services or selectors, to a standard HTTP response, you need to catch the exception and raise something that the framework understands.

[Django] The best place to do this is in the `handle_exception` method of the `APIView`. There you can map your Python/Django exception to a DRF exception.  
By default, the [`handle_exception` method implementation in DRF](https://www.django-rest-framework.org/api-guide/exceptions/#exception-handling-in-rest-framework-views) handles the Django's built-in `Http404` and `PermissionDenied` exceptions, thus there is no need for you to handle it by hand.

[Django] Here is an example:

```python
from rest_framework import exceptions as rest_exceptions

from django.core.exceptions import ValidationError


class CourseCreateApi(SomeAuthenticationMixin, APIView):
    expected_exceptions = {
        # Map non-DRF exception to a DRF exception.
        ValidationError: rest_exceptions.ValidationError,
    }

    class InputSerializer(serializers.Serializer):
        ...

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        create_course(**serializer.validated_data)

        return Response(status=status.HTTP_201_CREATED)

    def handle_exception(self, exc):
        if isinstance(exc, tuple(self.expected_exceptions.keys())):
            drf_exception_class = self.expected_exceptions[exc.__class__]
            drf_exception = drf_exception_class(get_error_message(exc))

            return super().handle_exception(drf_exception)

        return super().handle_exception(exc)
```

[Django] You can move this code to a mixin and use it in every API to prevent code duplication.  
We call this `ApiErrorsMixin`. Here's a sample implementation from one of our projects:

```python
from rest_framework import exceptions as rest_exceptions

from django.core.exceptions import ValidationError

from project.common.utils import get_error_message


class ApiErrorsMixin:
    """
    Mixin that transforms Django and Python exceptions into rest_framework ones.
    Without the mixin, they return 500 status code which is not desired.
    """
    expected_exceptions = {
        ValueError: rest_exceptions.ValidationError,
        ValidationError: rest_exceptions.ValidationError,
        PermissionError: rest_exceptions.PermissionDenied,
    }

    def handle_exception(self, exc):
        if isinstance(exc, tuple(self.expected_exceptions.keys())):
            drf_exception_class = self.expected_exceptions[exc.__class__]
            drf_exception = drf_exception_class(get_error_message(exc))

            return super().handle_exception(drf_exception)

        return super().handle_exception(exc)
```

[Django] Having this mixin in mind, our API can be written like this:

```python
class CourseCreateApi(
    SomeAuthenticationMixin,
    ApiErrorsMixin,
    APIView,
):
    class InputSerializer(serializers.Serializer):
        ...

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        create_course(**serializer.validated_data)

        return Response(status=status.HTTP_201_CREATED)
```

[Django] The mixin above should live under `your_app/api/mixins/<module>.py` or `your_app/api/v<X_Y>/mixins/<module>.py`.

[FastAPI] Given that FastAPI currently doesn't have class-based views, and that they wouldn't be so usefully due to the heavy use of decorators, you could catch known exceptions using an [exception handler](https://fastapi.tiangolo.com/tutorial/handling-errors/#install-custom-exception-handlers).  
Under `your_app/app/exceptions.py` do something like:

```python
from fastapi import Request
from fastapi.responses import ORJSONResponse

from ..utils.exceptions import get_error_message
from ..exceptions import ValidationError
from .asgi import app


@app.exception_handler(ValidationError)
async def validation_error_exception_handler(
        request: Request,
        exc: ValidationError,
) -> ORJSONResponse:
    """Handle ValidationError exceptions properly."""
    return ORJSONResponse(
        status_code=400,
        content={"message": get_error_message(exc)},
    )
```

[FastAPI] However if you need to catch a single, particular exception for any given view you simply catch the exception:

```python
from fastapi import HTTPException
from fastapi import status

from your_app import models
from your_app import schemas
from your_app.exceptions import ValidationError
from your_app.utils.exceptions import get_error_message
from .. import services

@router.post('/', response_model=schemas.CourseCreateResponse,
             status_code=status.HTTP_201_CREATED)
async def course_create(data: schemas.CourseCreateRequest) -> models.Course:
    """Create a course."""
    try:
        course = await services.course_create(**data.dict())
    except ValidationError as exc:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, get_error_message(exc))
    else:
        return course
```

[FastAPI] Or even better, you can use an inner function and a decorator (which could be in `your_app/api/utils.py`) such as:

```python
from fastapi import HTTPException
from fastapi import status

from your_app import models
from your_app import schemas
from your_app.exceptions import ValidationError
from ...utils import catch
from .. import services

@router.post('/', response_model=schemas.CourseCreateResponse,
             status_code=status.HTTP_201_CREATED)
async def course_create(data: schemas.CourseCreateRequest) -> models.Course:
    """Create a course."""

        @catch(ValidationError)
        async def inner(**kwars) -> models.Course:
            return await services.course_create(**kwargs)

    return inner(**data.dict())
```

[FastAPI] Here's the decorator:

```python
import functools
import typing

from fastapi import HTTPException
from fastapi import status

from ..utils.exceptions import get_error_message


def catch(
        exc_classes: typing.Union[
            typing.Type[Exception],
            typing.Tuple[typing.Type[Exception]],
        ],
        /,
        *,
        status_code: int = status.HTTP_400_BAD_REQUEST,
) -> typing.Callable:
    """Catch any exception of given class/es raising an HTTPException instead."""

    def decorator(function):
        """Catch any exception raising an HTTPException instead."""

        @functools.wraps(function)
        async def wrapper(*args, **kwargs):
            """Wrap decorated function."""
            try:
                return await function(*args, **kwargs)
            except exc_classes as exc:
                detail = get_error_message(exc) or None
                raise HTTPException(status_code=status_code, detail=detail) from exc

        return wrapper

    return decorator
```

Here's the implementation of `get_error_message` which could live i.e. under `your_app/utils/exceptions.py`.

```python
def get_first_matching_attr(obj, *attrs, default=None):
    for attr in attrs:
        if hasattr(obj, attr):
            return getattr(obj, attr)

    return default


def get_error_message(exc):
    if hasattr(exc, 'message_dict'):
        return exc.message_dict
    error_msg = get_first_matching_attr(exc, 'message', 'messages')

    if isinstance(error_msg, list):
        error_msg = ', '.join(error_msg)

    if error_msg is None:
        error_msg = str(exc)

    return error_msg
```

##### Error formatting

**ToDo**: we need to merge the way Pydantic and DRF shows errors and define everything related here, creating whichever helper function is needed.

#### Testing

In our projects, we split our tests depending on the type of code they represent.

Meaning, we generally have tests for models, services, selectors & APIs / views.

The file structure usually looks like this:

```
app_name
   ├── __init__.py
   └── tests
       ├── __init__.py
       ├── models
       │   └── __init__.py
       │   └── test_some_model_name.py
       ├── selectors
       │   └── __init__.py
       │   └── test_some_selector_name.py
       └── services
           ├── __init__.py
           └── test_some_service_name.py
```

##### Naming conventions

We follow 2 general naming conventions:

* The test file names should be `test_the_name_of_the_module_that_is_tested.py`
* The test case should be `class <Module>[<Submodule>]<TheNameOfTheThingThatIsTested>Tests(TestCase):` i.e. `class ModelsUserTests(TestCase):`
    * For *async* tests inherit from `IsolatedAsyncioTestCase` instead of plain `TestCase`.

For example if we have services for `user`:

```python
def user_create(*args, **kwargs):
    pass

def user_delete(*args, **kwargs):
    pass
```

We are going to have the following for file name:

```
app_name/tests/api/v<X_Y>/services/test_user.py
```

And the following for test case:

```python
class ServicesUserCreateTests(TestCase):
    pass
```

We did not use submodule name for the test case because `class ServicesUserUserCreateTests(TestCase):` is redundant and horrible to read.

For utility functions tests, we follow a similar pattern.

For example, if we have `app_name/utils.py`, then we are going to have `app_name/tests/test_utils.py` and place different test cases in that file.

If we are to split the `utils.py` module into submodules, the same will happen for the tests:

* `app_name/utils/files.py`
* `app_name/tests/utils/test_files.py`

We try to match the structure of our modules with the structure of their respective tests.

##### Testing services

Service tests are the most important tests in the project. Usually, those are the heaviest tests with most lines of code.

General rule of thumb for service tests:

* The tests should cover the business logic behind the services in an exhaustive manner.
* [Django] The tests SHOULD hit the database - creating & reading from it.
* [FastAPI] The tests COULD hit the database - creating & reading from it - unless they are using a CRUD.
* The tests SHOULD mock async task calls & everything that goes outside the project.

When creating the required state for a given test, one can use a combination of:

* Fakes (We recommend using [`faker`](https://github.com/joke2k/faker)).
* Other services, to create the required objects.
* Special test utility & helper methods.
* Factories (We recommend using [`factory_boy`](https://factoryboy.readthedocs.io/en/latest/orms.html)).
* [Django] Plain `Model.objects.create()` calls, if factories are not yet introduced in the project.
* [Django] Optionally use [`model_bakery`](https://github.com/model-bakers/model_bakery) that acts as factory and faker.

###### [Django] Example

**Lets take a look at a service example:**

```python
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from your_app.api.vX_Y.selectors.payments import get_items_for_user
from your_app.models.payments import Item, Payment
from your_app.tasks.payments import charge_payment


def buy_item(
    *,
    item: Item,
    user: User,
) -> Payment:
    if item in get_items_for_user(user=user):
        raise ValidationError(f'Item {item} already in {user} items.')

    payment = Payment.objects.create(
        item=item,
        user=user,
        successful=False,
    )

    charge_payment.delay(payment_id=payment.id)

    return payment

```

The service:

* Calls a selector for validation.
* Creates ORM object.
* Calls a task.

**These are the tests:**

```python
from unittest.mock import patch

from django.test import TestCase
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from your_app.api.vX_Y.services.payments import buy_item
from your_app.models.payments import Payment, Item


class ServicesPaymentsBuyItemTests(TestCase):  # This time we use module and submodule
    def setUp(self):
        self.user = User.objects.create_user(username='Test User')
        self.item = Item.objects.create(
            name='Test Item',
            description='Test Item description',
            price=10.15,
        )

        self.service = buy_item

    @patch('django_styleguide.payments.services.get_items_for_user')
    def test_buying_item_that_is_already_bought_fails(self, mock_get_items_for_user):
        """
        Since we already have tests for `get_items_for_user`,
        we can safely mock it here and give it a proper return value.
        """
        mock_get_items_for_user.return_value = [self.item]

        with self.assertRaises(ValidationError):
            self.service(user=self.user, item=self.item)

    @patch('django_styleguide.payments.services.charge_payment.delay')
    def test_buying_item_creates_a_payment_and_calls_charge_task(
        self,
        mock_charge_payment,
    ):
        self.assertEqual(0, Payment.objects.count())

        payment = self.service(user=self.user, item=self.item)

        self.assertEqual(1, Payment.objects.count())
        self.assertEqual(payment, Payment.objects.first())

        self.assertFalse(payment.successful)

        mock_charge_payment.assert_called()
```

###### [FastAPI] Example

**Lets take a look at a service example:**

```python
async def buy_item(
        *,
        db: Session,
        background_tasks: BackgroundTasks,
        item: Item,
        user: User,
) -> Payment:
    if item in await get_items_for_user(db=db, user=user):
        raise ValidationError(f'Item {item} already in {user} items.')

    payment = await crud.Payment(db).create(
        item=item,
        user=user,
        successful=False,
    )

    background_tasks.add_task(charge_payment, payment.id)

    return payment
```

The service:

* Calls a selector for validation.
* Uses the CRUD.
* Calls a background task.

To test an async function we need to create an inner test runner function because TestCase can't `await` coroutines yet.

**These are the tests:**

```python
from unittest import mock
from unittest import IsolatedAsyncioTestCase

from your_app.exceptions import ValidationError
from your_app.api.vX_Y.services import payments
from your_app.models.payments import Payment, Item


# This time we use module and submodule
class ServicesPaymentsBuyItemTests(IsolatedAsyncioTestCase):

    @mock.patch.object(payments, 'get_items_for_user')
    async def test_buying_item_that_is_already_bought_fails(
        self,
        mock_get_items_for_user,
    ):
        """
        Since we already have tests for `get_items_for_user`,
        we can safely mock it here and give it a proper return value.
        """
        item = mock.MagicMock()
        user = mock.MagicMock()
        db = mock.MagicMock()
        background_tasks = mock.MagicMock()
        mock_get_items_for_user.return_value = [item]

        with self.assertRaises(ValidationError):
            await payments.buy_items(db=db, background_tasks=background_tasks,
                                     user=user, item=item)
        mock_get_items_for_user.assert_called_once_with(db=db, user=user)
        background_tasks.add_task.assert_not_called()

    @mock.patch.object(payments, 'crud')
    @mock.patch.object(payments, 'get_items_for_user')
    @mock.patch.object(payments, 'charge_payment')
    def test_buying_item_creates_a_payment_and_calls_charge_task(
        self,
        mock_charge_payment,
        mock_get_items_for_user,
        mock_crud,
    ):
        item = mock.MagicMock()
        user = mock.MagicMock()
        db = mock.MagicMock()
        background_tasks = mock.MagicMock()
        payment_expected = mock.AsyncMock()
        payment_expected.id = 1
        mock_crud.Payment.return_value.create.return_value = payment_expected
        mock_get_items_for_user.return_value = []

        payment = await payments.buy_items(
            db=db,
            background_tasks=background_tasks,
            user=user,
            item=item,
        )
        self.assertEqual(payment, payment_expected)
        mock_crud.Payment.assert_called_with(db)
        mock_crud.Payment.return_value.create.assert_called(
            user=user,
            item=item,
            successfull=False,
        )
        background_tasks.add_task.assert_called_with(mock_charge_payment, 1)
```

Unlike with Django, since we use a CRUD, we simply mock the CRUD and test. Then for the CRUD we can test that the database is being correctly written. However, there's no harm in not mocking the CRUD and actually test for DB writes here.

##### Testing selectors

Testing selectors is also an important part of every project.

Sometimes, the selectors can be really straightforward, and if we have to "cut corners", we can omit those tests. But in the end, it's important to cover our selectors too.

###### [Django] Example

Lets take another look at our example selector:

```python
from django.contrib.auth.models import User

from django_styleguide.common.types import QuerySetType

from django_styleguide.payments.models import Item


def get_items_for_user(
    *,
    user: User
) -> QuerySetType[Item]:
    return Item.objects.filter(payments__user=user)
```

As you can see, this is a very straightforward & simple selector. We can easily cover that with 2 to 3 tests.

**Here are the tests:**

```python
from django.test import TestCase
from django.contrib.auth.models import User

from your_app.api.vX_Y.selectors.payments import get_items_for_user
from your_app.models.payments import Item, Payment


class SelectorPaymentsGetItemsForUserTests(TestCase):
    def test_selector_returns_nothing_for_user_without_items(self):
        """
        This is a "corner case" test.
        We should get nothing if the user has no items.
        """
        user = User.objects.create_user(username='Test User')

        expected = []
        result = list(get_items_for_user(user=user))

        self.assertEqual(expected, result)

    def test_selector_returns_item_for_user_with_that_item(self):
        """
        This test will fail in case we change the model structure.
        """
        user = User.objects.create_user(username='Test User')

        item = Item.objects.create(
            name='Test Item',
            description='Test Item description',
            price=10.15,
        )

        Payment.objects.create(
            item=item,
            user=user,
        )

        expected = [item]
        result = list(get_items_for_user(user=user))

        self.assertEqual(expected, result)
```

###### [FastAPI] Example

Let's take another look at our example selector:

```python
import typing

from your_app.models.user import User
from your_app import crud
from your_app.models.payments import Item
from your_app.models.payments import Payment


def get_items_for_user(
    *,
    db,
    user: User,
) -> typing.List[Item]:
    return crud.Item(db).read(
        filters=[
            Payment.user=user,
        ],
        joins=[
            Item.payments,
        ],
    )
```

As you can see, this is a very straightforward & simple selector. We can easily cover that with 2 to 3 tests.

**Here are the tests:**

```python
from unittest import TestCase, mock

from your_app.api.vX_Y.selectors import payments
from your_app.models.payments import Item, Payment
from your_app.models.user import User


class SelectorPaymentsGetItemsForUserTests(TestCase):
    @mock.patch.object(payments, 'crud')
    def test_selector_returns_nothing_for_user_without_items(
            self,
            mock_crud,
    ):
        db = mock.MagicMock()
        user = mock.MagicMock()
        mock_crud.Item.return_value.read.return_value = []

        items = payments.get_items_for_user(db=db, user=user)
        self.assertEqual(items, [])
        mock_crud.Item.assert_called_once_with(db)
        mock_crud.Item.return_value.read.assert_called_once_with(
            filters=[
                Payment.user=user,
            ],
            joins=[
                Item.payments,
            ],
        )
```

## HTTP Server

Use [Gunicorn](https://gunicorn.org/). For FastAPI, use it with [Uvicorn](https://www.uvicorn.org/) worker.

## Utils

Use these modules to for linting and static checking:

* flake8
* flake8-rst-docstrings
* flake8-import-order
* flake8-blind-except
* flake8-builtins
* flake8-logging-format
* flake8-docstrings
* flake8-quotes
* flake8-debugger
* flake8-bugbear
* flake8-comprehensions
* flake8-bandit
* flake8-commas
* flake8-eradicate
* flake8-broken-line
* pep8-naming
* flake8-string-format
* flake8-annotations-complexity
* flake8-executable
* pydocstyle
* bandit
* mccabe
* dlint

`poetry add --dev flake8 flake8-rst-docstrings flake8-import-order flake8-blind-except flake8-builtins flake8-logging-format flake8-docstrings flake8-quotes flake8-debugger flake8-bugbear flake8-comprehensions flake8-bandit flake8-eradicate flake8-commas flake8-broken-line pep8-naming flake8-string-format flake8-annotations-complexity flake8-executable pydocstyle bandit mccabe dlint`

Configuration for `flake8` (create a `.flake8` file in the project root):

```
[flake8]
rst-directives =
    # These are sorted alphabetically - but that does not matter
    autosummary,data,currentmodule,deprecated,
    glossary,moduleauthor,plot,testcode,
    versionadded,versionchanged,

rst-roles =
    attr,class,func,meth,mod,obj,ref,term,
    # C programming language:
    c:member,
    # Python programming language:
    py:func,py:mod,

max-line-length=89
show-source=true
enable-extensions=G
import-order-style=pycharm

# There's a bug in PyCodeStyle regarding positional-only arguments which is
# already fixed but a new release hasn't come out yet. So instead of marking
# EVERY pos-only args statement I ignore all E225 errors. Worst case is that we
# will introduce some real E225 errors but as soon as PyCodeStyle is updated I
# will remove this ignore and lint the code.
ignore=B008,B009,E225

exclude =
    migrations
    local_settings.py

per-file-ignores =
    */tests/*: D100, D101, D102, D103, D104

# Maximum Cyclomatic Complexity allowed
max-complexity = 5

# Set the application name
application-import-names=<package name>

# Set packages that belong to us
application-package-names=<our packages>, <another package of ours>
```

Use *flake8* by simply pointing it to the module such as `flake8 module_directory/`.

Use *pydocstyle* by simply pointing it to the module such as `pydocstyle --explain module_directory/`.

Use *bandit* directly (besides the flake8 plugin) by simply pointing it to the module such as `bandit -i -r -v module_directory/`, where `-i` is report even with low confidence, `-r` search recursively and `-v` to be verbose.

You MAY use [httpie](https://httpie.org/) to make requests to API's from your terminal. It's very easy to use:

```bash
:~$ http :8000/users/ name=hackan password=stJnWjPQ5PhbuQ-cbxLwqg
```

That line will POST to /users/ at port 8000 with the values `name` as `hackan` and `password` as `stJnWjPQ5PhbuQ-cbxLwqg`.

## IDE

This section is *not* mandatory and merely a recommendation. You can use [PyCharm](https://www.jetbrains.com/pycharm/download/) or [VisualStudioCode](https://code.visualstudio.com/).

It is recommended to mark as excluded your virtualenv directory. In PyCharm,
right-click your virtualenv directory and then, near the end of the context menu, *Mark Directory as* > *excluded*.

## MyPy

MyPy is a very important tool to ensure we keep the code correctly typed. Here's a basic configuration that pretty much covers all cases:

```ini
[mypy]
disallow_incomplete_defs = True
disallow_untyped_decorators = True
check_untyped_defs = True
pretty = True
show_error_codes = True
show_error_context = True
show_column_numbers = True
warn_redundant_casts = True
warn_unreachable = True
warn_unused_configs = True
warn_unused_ignores = True
```

Save it as `mypy.ini` in your project root and that's it! Simply run `mypy` (after installing it as any other Python package) against your source code directory.

## YAPF

["Yet Another Python Formatter"](https://github.com/google/yapf) is very similar to [black](https://github.com/psf/black), but it allows to set your own style. In case of discussion I would fall for black vs nothing. But, since we are defining things here and now, following is the corresponding YAPF configuration for the style we want (thanks to @erudin for creating it!). Save it as `.style.yapf`.

```
# The last version is 0.30.0
[style]
based_on_style = pep8

# Align closing bracket with visual indentation.
ALIGN_CLOSING_BRACKET_WITH_VISUAL_INDENT = True

# Allow dictionary keys to exist on multiple lines. For example:
#
# x = {
#     ('this is the first element of a tuple',
#      'this is the second element of a tuple'):
#          value,
# }
ALLOW_MULTILINE_DICTIONARY_KEYS = True

# Allow splits before the dictionary value.
ALLOW_SPLIT_BEFORE_DICT_VALUE = True

# Allow splitting before a default / named assignment in an argument list.
ALLOW_SPLIT_BEFORE_DEFAULT_OR_NAMED_ASSIGNS = True

# Let spacing indicate operator precedence. For example:
# a = 1 * 2 + 3 / 4
# b = 1 / 2 - 3 * 4
# c = (1 + 2) * (3 - 4)
# d = (1 - 2) / (3 + 4)
# e = 1 * 2 - 3
# f = 1 + 2 + 3 + 4
#
# will be formatted as follows to indicate precedence:
#
# a = 1*2 + 3/4
# b = 1/2 - 3*4
# c = (1+2) * (3-4)
# d = (1-2) / (3+4)
# e = 1*2 - 3
# f = 1 + 2 + 3 + 4
# We should review this setting after flake8 or pycodestyle
# is updated regarding issue with E226 (currently setting it
# raises E226).
ARITHMETIC_PRECEDENCE_INDICATION = False

# Insert a blank line before a 'def' or 'class' immediately nested
# within another 'def' or 'class'. For example:
#
#   class Foo:
#                      # <------ this blank line
#     def method():
#       ...
BLANK_LINE_BEFORE_NESTED_CLASS_OR_DEF = True

# Insert a blank line before a module docstring.
BLANK_LINE_BEFORE_MODULE_DOCSTRING = False

# Do not split consecutive brackets. Only relevant when
# dedent_closing_brackets is set. For example:
#
#    call_func_that_takes_a_dict(
#        {
#            'key1': 'value1',
#            'key2': 'value2',
#        }
#    )
#
# would reformat to:
#
#    call_func_that_takes_a_dict({
#        'key1': 'value1',
#        'key2': 'value2',
#    })
COALESCE_BRACKETS = True

# The column limit.
COLUMN_LIMIT = 99

# Put closing brackets on a separate line, dedented, if the bracketed
# expression can't fit in a single line. Applies to all kinds of
# brackets, including function definitions and calls. For example:
#
#   config = {
#       'key1': 'value1',
#       'key2': 'value2',
#   }        # <--- this bracket is dedented and on a separate line
#
#   time_series = self.remote_client.query_entity_counters(
#       entity='dev3246.region1',
#       key='dns.query_latency_tcp',
#       transform=Transformation.AVERAGE(window=timedelta(seconds=60)),
#       start_ts=now()-timedelta(days=3),
#       end_ts=now(),
#   )        # <--- this bracket is dedented and on a separate line
DEDENT_CLOSING_BRACKETS = True

# Place each dictionary entry onto its own line.
EACH_DICT_ENTRY_ON_SEPARATE_LINE = True

# Respect EACH_DICT_ENTRY_ON_SEPARATE_LINE even if the line is shorter
# than COLUMN_LIMIT.
FORCE_MULTILINE_DICT=True

# The i18n function call names. The presence of this function stops
# reformattting on that line, because the string it has cannot be moved
# away from the i18n comment.
I18N_FUNCTION_CALL = ["_", "ugettext", "gettext"]

# Indent the dictionary value if it cannot fit on the same line as the
# dictionary key. For example:
#
#   config = {
#       'key1':
#           'value1',
#       'key2': value1 +
#               value2,
#   }
INDENT_DICTIONARY_VALUE = True

# Insert a space between the ending comma and closing bracket of a list,
# etc.
SPACE_BETWEEN_ENDING_COMMA_AND_CLOSING_BRACKET = False

# Set to True to prefer spaces around the assignment operator for
# default or keyword arguments
SPACES_AROUND_DEFAULT_OR_NAMED_ASSIGN = False

# The number of spaces required before a trailing comment.
SPACES_BEFORE_COMMENT = 2

# Split before arguments if the argument list is terminated by a
# comma
SPLIT_ARGUMENTS_WHEN_COMMA_TERMINATED = True

# If a comma separated list (dict, list, tuple, or function def) is on
# a line that is too long, split such that all elements are on a
# single line.
SPLIT_ALL_COMMA_SEPARATED_VALUES = False

# Variation on SPLIT_ALL_COMMA_SEPARATED_VALUES in which, if a subexpression
# with a comma fits in its starting line, then the subexpression is not split.
# This avoids splits like the one for b in this code:
#
# abcdef(
#     aReallyLongThing: int,
#     b: [Int,
#         Int])
#
# With the new knob this is split as:
#
# abcdef(
#     aReallyLongThing: int,
#     b: [Int, Int])
SPLIT_ALL_TOP_LEVEL_COMMA_SEPARATED_VALUES = False

# Set to True to prefer splitting before +, -, *, /, //, or @
# rather than after.
SPLIT_BEFORE_ARITHMETIC_OPERATOR = True

# Set to True to prefer splitting before '&', '|' or '^' rather than
# after.
SPLIT_BEFORE_BITWISE_OPERATOR = True

# Split before the closing bracket if a list or dict literal doesn't fit
# on a single line.
SPLIT_BEFORE_CLOSING_BRACKET = True

# Split before the '.' if we need to split a longer expression:
# foo = ('This is a really long string: {}, {}, {}, {}'.format(a, b, c, d))
# would reformat to something like:
#  foo = ('This is a really long string: {}, {}, {}, {}'
#         .format(a, b, c, d))
SPLIT_BEFORE_DOT = True

# Split after the opening paren which surrounds an expression if it doesn't fit
# on a single line.
SPLIT_BEFORE_EXPRESSION_AFTER_OPENING_PAREN = True

# If an argument / parameter list is going to be split, then split
# before the first argument.
SPLIT_BEFORE_FIRST_ARGUMENT = True

# For list comprehensions and generator expressions with multiple clauses.
# For example:
#
# result = [
#     a_var + b_var for a_var in xrange(1000) for b_var in xrange(1000)
#     if a_var % b_var]
#
# would reformat to something like:
#
# result = [
#     a_var + b_var
#     for a_var in xrange(1000)
#     for b_var in xrange(1000)
#     if a_var % b_var]
SPLIT_COMPLEX_COMPREHENSION = True
```

Now, it has some issues with not adding commas at the end of a sentence (black also has this issue). So we can compliment this with a neat package named [add-trailing-comma](https://pypi.org/project/add-trailing-comma/): so you would run YAPF against your source directory, followed by add-trailing-comma.

To run it against a source directory, you have to use something like find: `find sourcedir/ -type f -name "*.py" -exec add-trailing-comma "{}" \+`

Example with YAPF: `yapf -r -vv -i sourcedir/ && find sourcedir/ -type f -name "*.py" -exec add-trailing-comma "{}" \+`
