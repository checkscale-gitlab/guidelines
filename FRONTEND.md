# Front-end Guidelines
  
The main idea of this document is to cover all front-end development related processes, from the minimum needed setup to common practices and concepts itself. 

It is not intended to be linear, use the index based on your needs. Also think of each category as a place where certain type of links can be added, and feel free to contribute. 

- [Front-end Guidelines](#front-end-guidelines)
  - [Environment setup](#environment-setup)
    - [NodeJS and NPM](#nodejs-and-npm)
    - [VSCode](#vscode)
    - [Terminal](#terminal)
  - [Development Criteria](#development-criteria)
    - [Performance](#performance)
    - [Accessibility (a11y)](#accessibility-a11y)
  - [Common tools and practices](#common-tools-and-practices)
    - [Linter](#linter)
    - [Prettier](#prettier)
    - [Testing](#testing)
  - [Svelte](#svelte)
    - [Routing](#routing)
    - [Preprocess](#preprocess)
      - [SASS (SCSS) and PostCSS](#sass-scss-and-postcss)
  - [Sapper](#sapper)
    - [Server-side rendering](#server-side-rendering)
    - [Prefetch, preload, and module context](#prefetch-preload-and-module-context)
    - [Authentication and SSR (server-side rendering)](#authentication-and-ssr-server-side-rendering)
    - [Debug network requests (Sapper)](#debug-network-requests-sapper)
    - [Additional resources](#additional-resources)
      - [Notions](#notions)
      - [Tools](#tools)

## Environment setup

### NodeJS and NPM

* [Official documentation about package-lock.json](https://docs.npmjs.com/files/package-lock.json)
* [Git merge driver for automatic merging of lockfiles](https://www.npmjs.com/package/npm-merge-driver)

### VSCode
[Visual Studio Code](https://code.visualstudio.com/) is one of the most used IDEs, made by Microsoft. Also, there is [VSCodium](https://vscodium.com/), a telemetry-free redistribution of VSCode itself.

One of its main advantages, apart from being a free software, are the available extensions created by the community.

Here is a list of useful extensions for development:

* [Official Svelte syntax highlight and intellisense](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode)
* [Svelte 3 Snippets for VS Code](https://marketplace.visualstudio.com/items?itemName=fivethree.vscode-svelte-snippets)
* [Multi or single line alphabetical sorter](https://marketplace.visualstudio.com/items?itemName=ue.alphabetical-sorter)
* [A basic HTML5 boilerplate snippet generator](https://marketplace.visualstudio.com/items?itemName=sidthesloth.html5-boilerplate)
* [GitLens: Supercharge the Git capabilities](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
* [Integrates hadolint: a Dockerfile linter](https://marketplace.visualstudio.com/items?itemName=exiasr.hadolint)

Always refer to readme of each extension in order to know how to properly use it.

### Terminal

Ideally, it should be your main tool, here we will help you to achieve that.

## Development Criteria

Regarding how to choose new technologies...

* Read and try to understand why their main idea was then created for
* Look and compare  at numerical benchmarks meanwhile evaluate their scope of use

Regarding NodeJS and byproducts...

* Get a brief of any project scripts with `npm run`
* Use `npm update` and resolve dependencies to keep updated 
* Avoid any non-development dependencies (i.e `dependencies` package.json field)
* Install package submodules individually whenever you can, using only what is needed
* Check bundle size, network load time, and deps composition when evaluating packages
* Make use of module bundlers and ease development in many ways
* Code should be tested (eventually)

Regarding Svelte and Sapper...

* When searching for Svelte components ensure [Sapper SSR capability](https://sapper.svelte.dev/docs#Making_a_component_SSR_compatible)
* Try to look at inner code of packaged components before using them
* Component names should be capitalized, meanwhile routes names should not
* Files and directories with a leading underscore do not create routes
* Both, components and routes, can be used as folders
* Take care of browser history API directly with [Sapper](https://sapper.svelte.dev/docs#goto_href_options)

Regarding HTML and CSS...

* Learn how to properly choose [values and units](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units)
* Research about an HTML element and so try to use them as intended
* Sort CSS rules according to the semantic order of the [DOM](https://en.wikipedia.org/wiki/Document_Object_Model)
* In case of preprocessors, setup [source maps](https://developers.google.com/web/tools/chrome-devtools/javascript/source-maps) for development stage
* Try not to override styles but instead try to keep [CSS specifity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity) low
* Use [WebP format](https://developers.google.com/speed/webp) whenever it is possible (see cwebp, dwebp)
* Explore automated tools in order to verify performance, [a11y](https://en.wikipedia.org/wiki/Computer_accessibility) and [SEO](https://en.wikipedia.org/wiki/Search_engine_optimization) issues

### Performance

* [Bundlephobia: Find out the cost of adding a new frontend dependency](https://bundlephobia.com/)
* [Lighthouse: Automated auditing, performance metrics, and best practices for the web](https://github.com/GoogleChromeLabs/lighthousebot)

### Accessibility (a11y)

Accessibility, sometimes abbreviated as the numeronym *a11y*, refers to the support for impaired people.

* [The A11Y Project](https://a11yproject.com/resources/)
* [Web Accessibility Evaluation Tools List](https://www.w3.org/WAI/ER/tools/)
* [AXE: Accessibility engine for automated Web UI testing](https://github.com/dequelabs/axe-core)
* [Microsoft Accessibility Insights for Web](https://github.com/microsoft/accessibility-insights-web)

## Common tools and practices

### Linter

Is any software tool that analyzes a source code to flag warnings or programming errors. It usually works in real time and it's available in different tools:

* Svelte [dev run script](https://github.com/sveltejs/sapper-template/blob/master/package_template.json#L6) comes with an in-built JS/HTML/CSS linter
* VSCode have [specific extensions](#vscode) for linting Svelte files
* As [a CLI script](#terminal) for your preferred terminal editor

### Prettier

Is not a specific kind of software but instead _an opinionated code formatter_ which let us [enforce to defined style guides](https://prettier.io/docs/en/why-prettier.html
).

Be aware that here we are not talking of _styles_ as the CSS language itself but instead as one of the many ambiguous ways one can adhere to write code using a javascript framework.

Their configuration typically resides in a `.prettierrc` file, and looks like the following:

```json
{
  "plugins": ["prettier-plugin-svelte"],
  "svelteSortOrder": "scripts-markup-styles",
  "singleQuote": true,
  "tabWidth": 2,
  "trailingComma": "es5"
}
```

Since prettier itself cover style rules for many frameworks, it first needs to be specified which of those are in our project, here we use the `plugins` field.

This plugin will then let us specify framework-related rules, like `svelteSortOder` as the order of different code blocks within any `.svelte` file.

### Testing

This is one of the less covered areas in Svelte since it lacks built-in support, but there are third party integrations:

* [Svelte Testing Library](https://testing-library.com/docs/svelte-testing-library/example)
* [How to test Svelte components with Jest](https://timdeschryver.dev/blog/how-to-test-svelte-components)

## Svelte

Svelte is the next-gen of frontend [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) which applications do not include framework references.   

* [Svelte REPL](https://svelte.dev/repl)
* [Svelte official chat](https://svelte.dev/chat)
* [Svelte official documentation](https://svelte.dev/docs)
* [Svelte/Sapper community resources](https://svelte-community.netlify.com)
* [Made with Svelte: Svelte Showcase](https://madewithsvelte.com)

### Routing

By default Svelte does not come with an built-in page router, but does Sapper.

Also there are plenty of yet-coded community driven implementations, as one can even make a custom component for that function. Such yet-coded good examples exist, like [routify](https://github.com/sveltech/routify).

### Preprocess

Regarding the main framework of choice, support for preprocessors vary from each module bundler to another. Svelte community has a preference for `rollup` over `webpack`, and still other solutions exist (like `svelvet`). The defacto package for this feature [`svelte-preprocess`](https://github.com/sveltejs/svelte-preprocess) and the following example shows its basic configuration in [`rollup.config.js`](https://gitlab.com/nevrona/public/skels/sapper/-/blob/master/rollup.config.js):

```js
// ... End of modules import
import sveltePreprocess from 'svelte-preprocess';

const preprocess = sveltePreprocess({
  // Specifics for the wanted preprocessor
});

export default {
  client: {
    ...
    plugins: [
      ...
      svelte({
        ...
        preprocess
      }),
      ...
    ],
    ...
  },
  // And, in case of Sapper, also...
  server: {
    ...
    plugins: [
      ...
      svelte({
        ...
        preprocess
      }),
      ...
    ],
    ...
  },
  ...
}
```

_The official preprocess library requires to call their object at end of `svelte` function parameters_

#### SASS (SCSS) and PostCSS

As mentioned below `sveltePreprocess` values are specific for the wanted preprocessor. 

The following example suffices for SASS:

```js
const preprocess = sveltePreprocess({
  scss: {
    renderSync: true,
    includePaths: ['src'],
    implementation: require('node-sass'),
  }
});
```

As you could see this examples requires that you install `node-sass` as a _devDependency_.

Now you can use the preprocessor within any `.svelte` file as the following:

```html
<style lang="scss">
  /* SASS rules */ 
</style>
```

By default, all styles within a Svelte component are local, and the compiler comes with an in-built way called as `:global()` to define global styles:

```html
<style>
  /* All styles are local by default */
  :global(body) {
    /* Global styles */
  }
</style>
```

_Instead `body` element can be used any kind of selectors._

What does PostCSS offer instead is also the inverse function for an `global` attribute:

```html
<style lang="scss" global>
  /* All styles are now global by default */
  :local(div) {
    /* Local styles */
  }
</style>
```

And a more elegant way of using `:global()` function directly as a selector:

```html
<style lang="scss">
  :global {
    @import ...
    body {
      ...
    }
  }
</style>
```

## Sapper

[Sapper](https://sapper.svelte.dev/docs), as a framework, is not an HTTP server by itself because by default leverages to [`polka`](https://www.npmjs.com/package/polka) (a minimal version of the well known [`express`](https://www.npmjs.com/package/express) package) and rather it's a middleware that offers us a variety of things:

* A filesystem-based route system
* Prefetching and preload functions
* Each page of your app is a Svelte component
* Access to global stores vía the _session_ object
* Server-side routes exposed as an internal JSON API

### Server-side rendering

This is a technique of executing code in the server to have a customized response ready for the client (i.e. the frontend of a web app).

It has an impact on performance since it's generally faster to make all the requests within a server than making extra browser-to-server round-trips for them.

[Server routes](https://sapper.svelte.dev/docs#Server_routes) are modules written in `.js` files that export functions corresponding to HTTP methods. Each function receives HTTP `request` and `response` objects as arguments, plus a `next` function. This is useful for creating a JSON API. 

### Prefetch, preload, and module context

Svelte files can have an additional script block with special properties, by passing `context="module"` to its tag, called `preload`. As the name suggests, this script runs before the component is created, and thus it's not part of the component instance itself.

It allows hydrating client-side with special objects, similar to `getInitialProps` in Next (React) or `asyncData` in Nuxt (Vue).

As one can specify scripts to be executed by the server using `module` context,  it is also possible to choose what does trigger such executions. This is what `prefetch` function does: adding `rel=prefech` attribute to a link (i.e. `a` elements) will cause Sapper to run the page's preload function as soon as the user hovers over the link (on a desktop) or touches it (on mobile). Typically, this gives us an extra couple of hundred milliseconds, which makes a difference in the performance of user experience.

### Authentication and SSR (server-side rendering)

When a page renders, session is populated according to the return value of the following function:
```js
// src/server.js
...
sapper.middleware({
  session: req => ({
    token: req.session && req.session.token
  })
})
```
So while the client may have an up-to-date token, it won't take effect on page reload unless you somehow persist the token to the server in such a way that the session middleware knows about it.

Typically, you'd achieve this by having a server route, like routes/auth/token.js or something...
```js
export function post(req, res) {
  req.session.token = req.body.token;

  res.writeHead(200, {
    'Content-Type': 'application/json'
  });

  res.end();
}
```
...and posting the token from the client:
```js
onMount(async () => {
  try {
    await client.reAuthenticate();
    const auth = await client.get('authentication');
    user.set(auth.user);

    await fetch(`auth/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ token })
    });

    // writing to the session store on the client means
    // it's immediately available to the rest of the app,
    // without needing to reload the page
    $session.token = 'test';
  } catch (e) {
  } finally {
    loaded = true;
  }
});
```
### Debug network requests (Sapper)

* _To-Do: Basic use of any HTTP request logger, like [Morgan](https://www.npmjs.com/package/morgan)._
* _To-Do: How to setup IntellijIDEA/[VSCode](https://) network debbuger for NodeJS._

### Additional resources

This place is intended for any resource which does not necessarily fit into any existing category. We can distinguish and group links as tools or concepts. 

#### Notions

* [Interneting Is Hard](https://www.internetingishard.com/)
* [MDN: Organizing your CSS](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Organizing)
* [Google Developers: Rendering on the Web](https://developers.google.com/web/updates/2019/02/rendering-on-the-web)
* [Source maps: languages, tools and other info](https://github.com/ryanseddon/source-map/wiki/Source-maps:-languages,-tools-and-other-info)

#### Tools

* [CSS Specificity Calculator](https://specificity.keegan.st/)
* [CSS Grid Generator](https://grid.layoutit.com/)
* [Web.dev Lighthouse audit](https://web.dev/measure/)
